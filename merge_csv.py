#!/usr/bin/env python

"""Merge and standardize Capital Bikeshare trip history files.

Usage:
1. Download trip history files from https://www.capitalbikeshare.com/system-data
2. Unzip each file to the same directory as this file
3. Modify CABI_CSV_FORMATS if file names or headers have changed (comment out lines to skip files)
4. python merge_csv.py history_merged.csv

history_merged.csv will contain the following columns:
* "Duration (ms)"
* "Start time": ISO 8601 format (YYYY-MM-DDTHH:MM:SS)
* "Start station"
* "End time": ISO 8601 format (YYYY-MM-DDTHH:MM:SS)
* "End station"
* "Bike number": may contain odd data (e.g. "? (0x74BEBCE4)"). These are errors
  in the original files.
* "Type": Terminology has changed over time, original values are preserved.

Station IDs are not included as they are not present in newer files and may
not have changed if a station moved.
"""

import abc
import argparse
import csv
import re
from collections import OrderedDict
from datetime import datetime

__author__ = "Dylan Barlett"
__copyright__ = "Arlington County Government"
__license__ = "GPLv3"
__version__ = "0.1"
__maintainer__ = "Dylan Barlett"
__email__ = "opendata@arlingtonva.us"
__status__ = "Development"

# Formats are named after the first file in which they appeared.
CABI_CSV_FORMATS = {
    "2010-Q4-cabi-trip-history-data.csv": "2010Q4",
    "2011-Q1-cabi-trip-history-data.csv": "2010Q4",
    "2011-Q2-cabi-trip-history-data.csv": "2010Q4",
    "2011-Q3-cabi-trip-history-data.csv": "2010Q4",
    "2011-Q4-cabi-trip-history-data.csv": "2010Q4",
    "2012-Q1-cabi-trip-history-data.csv": "2012Q1",
    "2012-Q2-cabi-trip-history-data.csv": "2012Q2",
    "2012-Q3-cabi-trip-history-data.csv": "2012Q3",
    "2012-Q4-cabi-trip-history-data.csv": "2012Q4",
    "2013-Q1-cabi-trip-history-data.csv": "2012Q4",
    "2013-Q2-cabi-trip-history-data.csv": "2013Q2",
    "2013-Q3-cabi-trip-history-data.csv": "2012Q4",
    "2013-Q4-cabi-trip-history-data.csv": "2013Q4",
    "2014-Q1-cabi-trip-history-data.csv": "2014Q1",
    "2014-Q2-cabi-trip-history-data.csv": "2014Q2",
    "2014-Q3-cabi-trip-history-data.csv": "2014Q3",
    "2014-Q4-cabi-trip-history-data.csv": "2014Q4",
    "2015-Q1-Trips-History-Data.csv": "2015Q1",
    "2015-Q2-Trips-History-Data.csv": "2015Q2",
    "2015-Q3-cabi-trip-history-data.csv": "2015Q3",
    "2015-Q4-Trips-History-Data.csv": "2015Q3",
    "2016-Q1-Trips-History-Data.csv": "2016Q1",
    "2016-Q2-Trips-History-Data.csv": "2016Q2",
    "2016-Q3-Trips-History-Data-1.csv": "2016Q1",
    "2016-Q3-Trips-History-Data-2.csv": "2016Q1",
    "2016-Q4-Trips-History-Data.csv": "2016Q1"
}


class TripHistoryParser(object):
    """Base class for trip history parsers.
    """
    field_names = [
        "Duration (ms)",
        "Start time",
        "Start station",
        "End time",
        "End station",
        "Bike number",
        "Type",
    ]

    # SQL-friendly field names
    field_names_sql = [
        "duration_ms",
        "start_time",
        "start_station",
        "end_time",
        "end_station",
        "bike_number",
        "type",
    ]

    @staticmethod
    def format_times_iso(trip):
        """Convert trip start/end times to ISO 8601 format for CSV output.

        :param trip: trip data
        :type trip: dict
        :returns: trip data, with start/end times in ISO 8601 format
        :rtype: dict
        """
        trip_iso = trip
        trip_iso["Start time"] = trip_iso["Start time"].isoformat()
        trip_iso["End time"] = trip_iso["End time"].isoformat()
        return trip_iso

    @abc.abstractmethod
    def parse_file(self, dict_reader):
        """This method must return a list of trip dicts, each with field_names as keys

        :param dict_reader: csv.DictReader object
        :type dict_reader: csv.DictReader
        :returns: list of trips (dicts)
        :rtype: list
        """
        return

    def parse_duration(self, duration):
        """Parse a CaBi trip duration from a Trip History Data file.

        Format: "14h 26min. 2sec.", "0h 16m 18s", or "2394764"

        :param duration: duration string from file
        :type duration: str
        :returns: trip duration, in milliseconds
        :rtype: int
        """
        if " " in duration:
            hours = int(re.search(r"\d{1,2}(?=h)", duration).group(0))
            minutes = int(re.search(r"\d{1,2}(?=m)", duration).group(0))
            seconds = int(re.search(r"\d{1,2}(?=s)", duration).group(0))
            return ((hours * 3600) + (minutes * 60) + seconds) * 1000
        else:
            return int(duration)

    def parse_station_name(self, name):
        """Parse a CaBi station name from a Trip History Data file.

        Format: "10th & U St NW (31111)" or "10th & U St NW"

        :param name: duration string from file
        :type name: str
        :returns: name, without ID
        :rtype: str
        """
        match = re.search(r"\((\d{5})\)$", name)
        if match:
            # Station ID is present (e.g. "10th & U St NW (31111)")
            station_name = name[:match.start()].strip()
        else:
            station_name = name
        return station_name


class TripHistoryParser2010Q4(TripHistoryParser):
    """Trip history parser for the 2010Q4 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = self.parse_duration(row["Duration"])
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike#"]
            trip["Type"] = row["Member Type"]
            trip["Start station"] = self.parse_station_name(row["Start station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End station"])
            trips.append(trip)
        return trips


class TripHistoryParser2012Q1(TripHistoryParser):
    """Trip history parser for the 2012Q1 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = self.parse_duration(row["Duration"])
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike#"]
            trip["Type"] = row["Type"]
            trip["Start station"] = self.parse_station_name(row["Start Station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End Station"])
            trips.append(trip)
        return trips


class TripHistoryParser2012Q2(TripHistoryParser):
    """Trip history parser for the 2012Q2 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = self.parse_duration(row["Duration"])
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike#"]
            trip["Type"] = row["Bike Key"]
            trip["Start station"] = self.parse_station_name(row["Start Station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End Station"])
            trips.append(trip)
        return trips


class TripHistoryParser2012Q3(TripHistoryParser):
    """Trip history parser for the 2012Q3 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = self.parse_duration(row["Duration"])
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike#"]
            trip["Type"] = row["Subscriber Type"]
            trip["Start station"] = self.parse_station_name(row["Start Station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End Station"])
            trips.append(trip)
        return trips


class TripHistoryParser2012Q4(TripHistoryParser):
    """Trip history parser for the 2012Q4 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = self.parse_duration(row["Duration"])
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike#"]
            trip["Type"] = row["Subscription Type"]
            trip["Start station"] = self.parse_station_name(row["Start Station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End Station"])
            trips.append(trip)
        return trips


class TripHistoryParser2013Q2(TripHistoryParser):
    """Trip history parser for the 2013Q2 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = self.parse_duration(row["Duration"])
            trip["Start time"] = datetime.strptime(row["Start time"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike#"]
            trip["Type"] = row["Subscription Type"]
            trip["Start station"] = self.parse_station_name(row["Start Station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End Station"])
            trips.append(trip)
        return trips


class TripHistoryParser2013Q4(TripHistoryParser):
    """Trip history parser for the 2013Q4 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = self.parse_duration(row["Duration"])
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike#"]
            trip["Type"] = row["Subscription Type"]
            trip["Start station"] = self.parse_station_name(row["Start Station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End Station"])
            trips.append(trip)
        return trips


class TripHistoryParser2014Q1(TripHistoryParser):
    """Trip history parser for the 2014Q1 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = self.parse_duration(row["Duration"])
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike#"]
            trip["Type"] = row["Member Type"]
            trip["Start station"] = self.parse_station_name(row["Start Station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End Station"])
            trips.append(trip)
        return trips


class TripHistoryParser2014Q2(TripHistoryParser):
    """Trip history parser for the 2014Q2 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = self.parse_duration(row["Duration"])
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike#"]
            trip["Type"] = row["Subscriber Type"]
            trip["Start station"] = self.parse_station_name(row["Start Station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End Station"])
            trips.append(trip)
        return trips


class TripHistoryParser2014Q3(TripHistoryParser):
    """Trip history parser for the 2014Q3 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = self.parse_duration(row["Duration"])
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike#"]
            trip["Type"] = row["Subscription Type"]
            trip["Start station"] = self.parse_station_name(row["Start Station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End Station"])
            trips.append(trip)
        return trips


class TripHistoryParser2014Q4(TripHistoryParser):
    """Trip history parser for the 2014Q4 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = self.parse_duration(row["Duration"])
            trip["Start time"] = datetime.strptime(row["Start date"], "%Y-%m-%d %H:%M")
            trip["Bike number"] = row["Bike#"]
            trip["Type"] = row["Subscription Type"]
            trip["Start station"] = self.parse_station_name(row["Start Station"])
            trip["End time"] = datetime.strptime(row["End date"], "%Y-%m-%d %H:%M")
            trip["End station"] = self.parse_station_name(row["End Station"])
            trips.append(trip)
        return trips


class TripHistoryParser2015Q1(TripHistoryParser):
    """Trip history parser for the 2015Q1 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = row["Total duration (ms)"]
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike number"]
            trip["Type"] = row["Subscription Type"]
            trip["Start station"] = self.parse_station_name(row["Start station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End station"])
            trips.append(trip)
        return trips


class TripHistoryParser2015Q2(TripHistoryParser):
    """Trip history parser for the 2015Q2 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = row["Duration (ms)"]
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike number"]
            trip["Type"] = row["Subscription type"]
            trip["Start station"] = self.parse_station_name(row["Start station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End station"])
            trips.append(trip)
        return trips


class TripHistoryParser2015Q3(TripHistoryParser):
    """Trip history parser for the 2015Q2 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = row["Duration (ms)"]
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike #"]
            trip["Type"] = row["Member type"]
            trip["Start station"] = self.parse_station_name(row["Start station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End station"])
            trips.append(trip)
        return trips


class TripHistoryParser2016Q1(TripHistoryParser):
    """Trip history parser for the 2016Q1 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = row["Duration (ms)"]
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike number"]
            trip["Type"] = row["Member Type"]
            trip["Start station"] = self.parse_station_name(row["Start station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End station"])
            trips.append(trip)
        return trips


class TripHistoryParser2016Q2(TripHistoryParser):
    """Trip history parser for the 2016Q2 file format.
    """
    def parse_file(self, dict_reader):
        trips = []
        for row in dict_reader:
            trip = {}
            trip["Duration (ms)"] = row["Duration (ms)"]
            trip["Start time"] = datetime.strptime(row["Start date"], "%m/%d/%Y %H:%M")
            trip["Bike number"] = row["Bike number"]
            trip["Type"] = row["Account type"]
            trip["Start station"] = self.parse_station_name(row["Start station"])
            trip["End time"] = datetime.strptime(row["End date"], "%m/%d/%Y %H:%M")
            trip["End station"] = self.parse_station_name(row["End station"])
            trips.append(trip)
        return trips

if __name__ == '__main__':
    parsers = {
        "2010Q4": TripHistoryParser2010Q4(),
        "2012Q1": TripHistoryParser2012Q1(),
        "2012Q2": TripHistoryParser2012Q2(),
        "2012Q3": TripHistoryParser2012Q3(),
        "2012Q4": TripHistoryParser2012Q4(),
        "2013Q2": TripHistoryParser2013Q2(),
        "2013Q4": TripHistoryParser2013Q4(),
        "2014Q1": TripHistoryParser2014Q1(),
        "2014Q2": TripHistoryParser2014Q2(),
        "2014Q3": TripHistoryParser2014Q3(),
        "2014Q4": TripHistoryParser2014Q4(),
        "2015Q1": TripHistoryParser2015Q1(),
        "2015Q2": TripHistoryParser2015Q2(),
        "2015Q3": TripHistoryParser2015Q3(),
        "2016Q1": TripHistoryParser2016Q1(),
        "2016Q2": TripHistoryParser2016Q2(),
    }

    argparser = argparse.ArgumentParser()
    argparser.add_argument("outfile", help="output CSV file")
    argparser.add_argument(
        "--header",
        choices=["sql", "none"],
        help="output SQL-friendly (no spaces) CSV header or omit it"
    )
    args = argparser.parse_args()

    with open(args.outfile, "wb") as outfile:
        writer = csv.DictWriter(
            outfile,
            fieldnames=TripHistoryParser.field_names
        )
        if args.header == "sql":
            writer.writerow(dict(zip(
                TripHistoryParser.field_names,
                TripHistoryParser.field_names_sql
            )))
        elif args.header == "none":
            pass
        else:
            writer.writeheader()
        # Process files in lexicographical order
        files_formats = OrderedDict((k, CABI_CSV_FORMATS[k]) for k in sorted(CABI_CSV_FORMATS.keys()))
        for filename, file_format in files_formats.items():
            try:
                with open(filename) as csvfile:
                    print "Processing", filename
                    reader = csv.DictReader(csvfile)
                    trips = parsers[file_format].parse_file(reader)
                    for trip in trips:
                        writer.writerow(TripHistoryParser.format_times_iso(trip))
                    print "Finished", filename
            except IOError:
                print "Error reading {}, skipping".format(filename)
